import Vue from 'vue';
import './plugins/vuetify'
import firebase from 'firebase/app';
import App from './App.vue';
import router from './router';
import Vuetify from 'vuetify';

Vue.config.productionTip = false;


let app = '';
const config = {
  apiKey: "AIzaSyDNlv-4OsmAKiFqfX-FRxm0azCCxCW33Uk",
  authDomain: "viewster-df660.firebaseapp.com",
  databaseURL: "https://viewster-df660.firebaseio.com",
  projectId: "viewster-df660",
  storageBucket: "viewster-df660.appspot.com",
  messagingSenderId: "1072911938901",
  appId: "1:1072911938901:web:9bceb94a0691dce8"
};

firebase.initializeApp(config);

var profilephoto = "http://ec2-18-224-202-151.us-east-2.compute.amazonaws.com/img/img_avatar.a5e81f19.png";

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    /* eslint-disable no-new */
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount('#app');
  }
});



const db = firebase.firestore();
const storage = firebase.storage();

var fire =  {db, storage, firebase, profilephoto};
export default fire;


Vue.use(Vuetify, {
  theme: {
    primary: '#8E24AA'
  }
})